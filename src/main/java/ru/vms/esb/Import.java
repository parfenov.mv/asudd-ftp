package ru.vms.esb;

import java.io.File;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.processor.idempotent.FileIdempotentRepository;
import org.apache.camel.spi.IdempotentRepository;
import ru.vms.esb.JsonTransformator;

public class Import extends RouteBuilder {
	
	
	private IdempotentRepository<String> idempotentRepo = FileIdempotentRepository.fileIdempotentRepository(new File("data", "asudd-repo.dat"));
	private JsonTransformator jt = new JsonTransformator();
	
		
	public void configure() {
		
		//Create PropertiesComponent for using placeholder in route
		PropertiesComponent pc = new PropertiesComponent();
		pc.setLocation("file:${karaf.home}/etc/asudd.properties");
		getContext().addComponent("properties", pc);
		
		//extract data from ftp
		from("ftp://{{ftp.user}}@{{ftp.url}}?password={{ftp.pass}}&charset={{ftp.charset}}&passiveMode=true&noop=true&consumer.delay={{ftp.consumer.delay}}&recursive=true&filter=#fileFilter&maximumReconnectAttempts={{ftp.maxreconnect}}&reconnectDelay={{ftp.reconnectdelay}}")
			.routeId("vms.get.asudd.data")
			.to("direct:asudd.all.data");
		
		//filter for already loaded data
		from("direct:asudd.all.data")
			.routeId("vms.filter.data")
			.idempotentConsumer(header("CamelFileName"), idempotentRepo)
			.to("direct:asudd.filtered.data");
		
		//transform input xml to list of json
		from("direct:asudd.filtered.data")
			.routeId("vms.transform.data")
			.log("transform file: ${headers.CamelFileName}")
			.log("file content: ${body}")
			.bean(jt, "transform")
			.to("direct:asudd.json.save");
		
		//saving data via our rest-service
		from("direct:asudd.json.save")
	 		.routeId("vms.json.save")
	 		.log("saving json ${body}")
	 		.removeHeaders("*")
	 		.convertBodyTo(String.class, "utf-8")
	 		.setHeader("Content-Type", constant("application/json;charset=UTF-8"))
	 		.setHeader("CamelHttpMethod", constant("POST"))
	 		.to("jetty:{{vms.address}}{{vms.service.asudd}}{{vms.method.asudd}}")
	 		.log("response ${body}")
	 		.log("saving item complete");

	}

}
