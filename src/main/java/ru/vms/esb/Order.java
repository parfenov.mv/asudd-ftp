package ru.vms.esb;

import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONObject;

/**
 * Вспомогательный объект, который будет содержать коллекцию объектов, для легкого split-a
 * @author parfenov
 *
 */
public class Order {

	    private List orderItems = new ArrayList();

	    public void addItem(String item) {
	        this.orderItems.add(item);
	    }

	    public List getItems() {
	        return this.orderItems;
	    }
	    
	    public String toString() {
	        return "Order of Items: " + orderItems;
	    }
	}
