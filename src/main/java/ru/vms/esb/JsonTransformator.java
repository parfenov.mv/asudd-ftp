package ru.vms.esb;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.apache.camel.component.file.GenericFile;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

public class JsonTransformator {
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	private static final SimpleDateFormat dateFormatPM = new SimpleDateFormat("MMM dd, yyyy h:mm:ss a", Locale.US);
	private static final String templatePackage = "[{\"password\":\"9v3/5IyQjesPTDvTbAMucg==\", \"userName\":\"admin\"}, [%s]]";
	private static final String templateItem = "{\"category_code\":\"%s\", \"date\":\"%s\", \"line_a\":%s, \"line_b\":%s, \"speed_a\":%s, \"speed_b\":%s, \"stationAsudd_code\":\"%s\"}";
	
	/**
	 * Transformation asudd xml to list of json
	 * @param body
	 * @return
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ParseException 
	 */
	public Object transform(Object body) throws ParserConfigurationException, SAXException, IOException, ParseException {
		
		ByteArrayOutputStream message = (ByteArrayOutputStream)((GenericFile)body).getBody();
		ByteArrayInputStream stream = new ByteArrayInputStream(message.toByteArray());
			
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document document = documentBuilder.parse(stream);
			
		List<String> order = new ArrayList<String>();
		
		String sensorId = GetNodeTextValue(document.getDocumentElement(), "sensor_id");
		String speedA = GetNodeTextValue(document.getDocumentElement(), "speed_a");
		String speedB = GetNodeTextValue(document.getDocumentElement(), "speed_b");
		String time = GetNodeTextValue(document.getDocumentElement(), "time");
		Date date = dateFormat.parse(time);
		String datePM = dateFormatPM.format(date);
		
		NodeList nodes = document.getElementsByTagName("volume");

        if (nodes != null) {
            for (int i = 0; i < nodes.getLength(); i++) {
            	Element element = (Element) nodes.item(i);
            	String categoryId = GetNodeTextValue(element, "category_id");
            	String lineA = GetNodeTextValue(element, "line_a");
            	String lineB = GetNodeTextValue(element, "line_b");    	
            	if (!lineA.equals("0") || !lineB.equals("0")) {
            		String json = String.format(templateItem, categoryId, datePM, lineA, lineB, speedA, speedB, sensorId);
            		order.add(json);
            	}
            }
        }
        return String.format(templatePackage, String.join(",\r\n", order));        
	}
	
	/**
	 * Read text node value first find node by name
	 * @param root
	 * @param nodeName
	 * @return
	 */
	private String GetNodeTextValue(Element root, String nodeName) {
		String result = null;
		
		NodeList nodes = root.getElementsByTagName(nodeName);
		if (nodes != null) {
			Node node = (Node)nodes.item(0);
			
			if (node != null) {
				result = node.getTextContent();
			}
		}
		return result;
	}
}
