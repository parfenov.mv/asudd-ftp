package ru.vms.esb;

import org.apache.camel.component.file.GenericFile;
import org.apache.camel.component.file.GenericFileFilter;

public class FileFilter<T> implements GenericFileFilter<T> {
    public boolean accept(GenericFile<T> file) {
        return (!file.getFileName().contains("traficam"));
    }

}